import infrastructure.configuration.Config
import infrastructure.server.VertxServer
import com.sksamuel.hoplite.ConfigLoader
import infrastructure.server.HandlerDI
import infrastructure.server.TwitterDI

fun main() {
    val config = ConfigLoader().loadConfigOrThrow<Config>("/config.yml")
    val twitterDI = TwitterDI(config.twitterServerConfig)
    twitterDI.getDatabaseInitializer().openConnection()
    val server = VertxServer(config.serverConfiguration, HandlerDI(twitterDI))

    server.start()
}
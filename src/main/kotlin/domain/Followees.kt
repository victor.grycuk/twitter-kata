package domain

data class Followees(val follower: String, val followees: List<Followee> = emptyList())
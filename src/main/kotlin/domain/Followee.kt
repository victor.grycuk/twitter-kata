package domain

data class Followee(val nickname: String)
package domain

data class Twit(val nickname: String, val message: String)
package domain

interface UserRepository {
    fun add(user: User)
    fun findByNickname(nickname: String): User?
    fun checkIfUserExists(nickname: String): Boolean
    fun updateUser(user: User)
}
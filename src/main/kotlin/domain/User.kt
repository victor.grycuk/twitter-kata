package domain

data class User(val name: String, val nickname: String)
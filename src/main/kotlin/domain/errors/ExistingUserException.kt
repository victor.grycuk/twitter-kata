package domain.errors

class ExistingUserException(message: String): Exception(message)
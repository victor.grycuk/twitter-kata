package domain.errors

class UserNotFoundException(message: String): Exception(message)
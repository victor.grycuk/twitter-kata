package domain

interface TwitRepository {
    fun add(twit: Twit)
    fun findByUser(nickname: String): List<Twit>
    fun findByID(id: Int): Twit
}

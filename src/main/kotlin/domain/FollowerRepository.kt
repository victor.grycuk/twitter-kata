package domain

interface FollowerRepository {
    fun followUser(followerID: String, followeeID: String)
    fun getFolloweesByUser(followerID: String): Followees
}
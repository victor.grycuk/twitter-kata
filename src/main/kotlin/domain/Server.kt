package domain

interface Server {
    fun start()
}
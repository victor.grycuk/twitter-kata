package infrastructure.server

import domain.Server
import infrastructure.configuration.ServerConfiguration
import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler

class VertxServer(private val serverConfig: ServerConfiguration, private val handlerDI: HandlerDI) : Server {
    private val vertx: Vertx = Vertx.vertx()
    private var httpServer = vertx.createHttpServer()

    override fun start() {
        httpServer
            .requestHandler(getRouter(vertx))
            .listen(serverConfig.port)
    }

    fun close() {
        httpServer.close()
    }

    private fun getRouter(vertx: Vertx): Router = Router.router(vertx).apply {
        route().handler(BodyHandler.create())
        post(Endpoints.registerUser).handler(handlerDI.getRegisterHandler())
        get(Endpoints.findUser).handler(handlerDI.getFindHandler())
        put(Endpoints.updateUser).handler(handlerDI.getUpdateHandler())
        put(Endpoints.followUser).handler(handlerDI.getFollowHandler())

        get(Endpoints.getFollowees).handler(handlerDI.getGetFolloweesHandler())

        post(Endpoints.twitMessage).handler(handlerDI.getTwitHandler())
        get(Endpoints.getTwits).handler(handlerDI.getGetTwitsHandler())
    }
}
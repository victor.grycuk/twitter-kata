package infrastructure.server

class Endpoints {
    companion object {
        const val registerUser = "/users"
        const val findUser = "/users/:nickname"
        const val updateUser = "/users"
        const val followUser = "/users/follow"
        const val getFollowees = "/followers/:nickname"
        const val twitMessage = "/twits"
        const val getTwits = "/twits/:nickname"
    }
}
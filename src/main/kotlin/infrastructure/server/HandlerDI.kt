package infrastructure.server

import infrastructure.server.handlers.*
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

class HandlerDI(private val twitterDI: TwitterDI) {
    fun getRegisterHandler(): Handler<RoutingContext> = RegisterUserHandler(twitterDI.getRegisterUserAction())
    fun getFindHandler(): Handler<RoutingContext> = FindUserHandler(twitterDI.getFindUserByNicknameAction())
    fun getUpdateHandler(): Handler<RoutingContext> = UpdateUserHandler(twitterDI.getUpdateUserAction())
    fun getFollowHandler(): Handler<RoutingContext> = FollowUserHandler(twitterDI.getFollowUserAction())
    fun getGetFolloweesHandler(): Handler<RoutingContext> = RetrieveFolloweesHandler(twitterDI.getRetrieveFolloweesAction())
    fun getTwitHandler(): Handler<RoutingContext> = TwitMessageHandler(twitterDI.getTwitMessageAction())
    fun getGetTwitsHandler(): Handler<RoutingContext> = RetrieveTwitsHandler(twitterDI.getRetrieveTwitsByUserAction())
}
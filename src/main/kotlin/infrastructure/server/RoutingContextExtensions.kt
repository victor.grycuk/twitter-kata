package infrastructure.server

import com.google.gson.Gson
import domain.User
import infrastructure.server.handlers.FollowUserHandler
import infrastructure.server.handlers.TwitMessageHandler
import infrastructure.server.handlers.UpdateUserHandler
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.ext.web.RoutingContext

fun RoutingContext.getNewUserRequest(): User {
    return Gson().fromJson(this.body.toString(), User::class.java)
}

fun RoutingContext.getFollowRequest(): FollowUserHandler.FollowRequest {
    return Gson().fromJson(this.body.toString(), FollowUserHandler.FollowRequest::class.java)
}

fun RoutingContext.getTwitRequest(): TwitMessageHandler.TwitRequest {
    return Gson().fromJson(this.body.toString(), TwitMessageHandler.TwitRequest::class.java)
}

fun RoutingContext.getUpdateUserRequest(): UpdateUserHandler.UpdateRequest {
    return Gson().fromJson(this.body.toString(), UpdateUserHandler.UpdateRequest::class.java)
}

fun RoutingContext.getParameters(): Parameters {
    val nickname = this.request().getParam(Parameter.NICKNAME)
    return Parameters(nickname)
}

fun RoutingContext.respondOK(message: String) {
    this
        .response()
        .setStatusCode(HttpResponseStatus.OK.code())
        .end(message)
}

fun RoutingContext.respondOKWithContent(message: String) {
    this
        .response()
        .setStatusCode(HttpResponseStatus.OK.code())
        .putHeader("content-type", "application/json")
        .end(message)
}

fun RoutingContext.respondNotFound(message: String?) {
    this
        .response()
        .setStatusCode(HttpResponseStatus.NOT_FOUND.code())
        .end(message ?: "User not found")
}

fun RoutingContext.respondBadRequest(message: String?) {
    this
        .response()
        .setStatusCode(HttpResponseStatus.BAD_REQUEST.code())
        .end(message)
}

fun RoutingContext.respondInternalServerError(message: String?) {
    this
        .response()
        .setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code())
        .end(message ?: "Internal Server Error")
}
package infrastructure.server

import action.*
import domain.FollowerRepository
import domain.TwitRepository
import domain.UserRepository
import infrastructure.configuration.RepositoryType
import infrastructure.configuration.TwitterServerConfig
import infrastructure.repositories.inmemory.InMemoryFollowerRepository
import infrastructure.repositories.inmemory.InMemoryTwitRepository
import infrastructure.repositories.inmemory.InMemoryUserRepository
import infrastructure.repositories.mysql.DatabaseInitializer
import infrastructure.repositories.mysql.MySqlFollowerRepository
import infrastructure.repositories.mysql.MySqlTwitRepository
import infrastructure.repositories.mysql.MySqlUserRepository

class TwitterDI(private val twitterServerConfig: TwitterServerConfig) {
    private val userRepository: UserRepository = initUserRepository()
    private val twitRepository: TwitRepository = initTwitRepository()
    private val followerRepository: FollowerRepository = initFollowerRepository()

    private fun initUserRepository(): UserRepository {
        return when(twitterServerConfig.type) {
            RepositoryType.IN_MEMORY -> InMemoryUserRepository()
            RepositoryType.MYSQL -> MySqlUserRepository()
        }
    }

    private fun initTwitRepository(): TwitRepository {
        return when(twitterServerConfig.type) {
            RepositoryType.IN_MEMORY -> InMemoryTwitRepository()
            RepositoryType.MYSQL -> MySqlTwitRepository()
        }
    }

    private fun initFollowerRepository(): FollowerRepository {
        return when(twitterServerConfig.type) {
            RepositoryType.IN_MEMORY -> InMemoryFollowerRepository()
            RepositoryType.MYSQL -> MySqlFollowerRepository()
        }
    }

    fun getDatabaseInitializer() = DatabaseInitializer(twitterServerConfig)
    fun getRegisterUserAction() = RegisterUser(userRepository)
    fun getFindUserByNicknameAction() = FindUserByNickname(userRepository)
    fun getUpdateUserAction() = UpdateUser(userRepository)
    fun getFollowUserAction() = FollowUser(followerRepository, userRepository)
    fun getRetrieveFolloweesAction() = RetrieveFolloweesByUser(followerRepository, userRepository)
    fun getTwitMessageAction() = TwitMessage(twitRepository, userRepository)
    fun getRetrieveTwitsByUserAction() = RetrieveTwitsByUser(twitRepository, userRepository)
}
package infrastructure.server.handlers

import action.TwitMessage
import domain.errors.UserNotFoundException
import infrastructure.server.*
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

class TwitMessageHandler(private val twitMessage: TwitMessage): Handler<RoutingContext> {
    override fun handle(event: RoutingContext) {
        try {
            val form = event.getTwitRequest()
            if (form.nickname.isEmpty())
                return event.respondBadRequest("Parameter '${ TwitRequest::nickname.name }' cannot be empty.")

            if (form.message.isEmpty())
                return event.respondBadRequest("Parameter '${ TwitRequest::message.name }' cannot be empty.")

            twitMessage(form.nickname, form.message)

            event.respondOK("The message has been posted successfully")
        }
        catch (ex: UserNotFoundException) {
            event.respondNotFound(ex.message)
        }
        catch(ex: Exception) {
            event.respondInternalServerError(ex.message)
        }
    }

    data class TwitRequest(val nickname: String, val message: String)
}
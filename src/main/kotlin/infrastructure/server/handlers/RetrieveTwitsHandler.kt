package infrastructure.server.handlers

import action.RetrieveTwitsByUser
import domain.errors.UserNotFoundException
import infrastructure.server.*
import io.vertx.core.Handler
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext

class RetrieveTwitsHandler(private val retrieveTwits: RetrieveTwitsByUser): Handler<RoutingContext> {
    override fun handle(event: RoutingContext) {
        try {
            val parameters = event.getParameters()

            if (parameters.nickname.isEmpty())
                return event.respondBadRequest("Parameter '${ Parameter.NICKNAME }' cannot be empty.")

            val messages = retrieveTwits(parameters.nickname)

            event.respondOKWithContent(Json.encodePrettily(messages))
        }
        catch (ex: UserNotFoundException) {
            event.respondNotFound(ex.message)
        }
        catch(ex: Exception) {
            event.respondInternalServerError(ex.message)
        }
    }
}
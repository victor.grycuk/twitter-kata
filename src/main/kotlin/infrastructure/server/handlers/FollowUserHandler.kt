package infrastructure.server.handlers

import action.FollowUser
import domain.errors.UserNotFoundException
import infrastructure.server.*
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

class FollowUserHandler(private val followUser: FollowUser): Handler<RoutingContext> {
    override fun handle(event: RoutingContext) {
        try {
            val followForm = event.getFollowRequest()
            if (followForm.nickname.isEmpty())
                return event.respondBadRequest("Field '${ FollowRequest::nickname.name }' cannot be empty.")

            if (followForm.followee.isEmpty())
                return event.respondBadRequest("Field '${ FollowRequest::followee.name }' cannot be empty.")

            followUser(followForm.nickname, followForm.followee)

            event.respondOK("User '${ followForm.nickname }' now is following user '${ followForm.followee }'.")
        }
        catch (ex: UserNotFoundException) {
            event.respondNotFound(ex.message)
        }
        catch(ex: Exception) {
            event.respondInternalServerError(ex.message)
        }
    }

    data class FollowRequest(val nickname: String, val followee: String)
}
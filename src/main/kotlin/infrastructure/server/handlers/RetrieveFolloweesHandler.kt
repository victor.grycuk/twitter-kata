package infrastructure.server.handlers

import action.RetrieveFolloweesByUser
import domain.errors.UserNotFoundException
import infrastructure.server.*
import io.vertx.core.Handler
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext

class RetrieveFolloweesHandler(private val retrieveFolloweesByUser: RetrieveFolloweesByUser): Handler<RoutingContext> {
    override fun handle(event: RoutingContext) {
        try {
            val parameters = event.getParameters()
            if (parameters.nickname.isEmpty())
                return event.respondBadRequest("Parameter '${ Parameter.NICKNAME }' cannot be empty.")

            val followees = retrieveFolloweesByUser(parameters.nickname)

            event.respondOK(Json.encodePrettily(followees))
        }
        catch(ex: UserNotFoundException) {
            event.respondNotFound(ex.message)
        }
        catch(ex: Exception) {
            event.respondInternalServerError(ex.message)
        }
    }
}
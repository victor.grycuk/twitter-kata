package infrastructure.server.handlers

import action.UpdateUser
import domain.errors.UserNotFoundException
import infrastructure.server.*
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

class UpdateUserHandler(private val updateUser: UpdateUser): Handler<RoutingContext> {
    override fun handle(event: RoutingContext) {
        try {
            val user = event.getUpdateUserRequest()

            if (user.newName.isEmpty())
                return event.respondBadRequest("Parameter '${ UpdateRequest::newName.name }' cannot be empty.")

            updateUser(user.nickname, user.newName)

            event.respondOK("User updated successfully")
        }
        catch (ex: UserNotFoundException) {
            event.respondNotFound(ex.message)
        }
        catch (ex: Exception) {
            event.respondInternalServerError(ex.message)
        }
    }

    data class UpdateRequest(val nickname: String, val newName: String)
}
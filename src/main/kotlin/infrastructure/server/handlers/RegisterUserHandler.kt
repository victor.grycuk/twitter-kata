package infrastructure.server.handlers

import action.RegisterUser
import domain.User
import domain.errors.ExistingUserException
import infrastructure.server.*
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

class RegisterUserHandler(private val registerUser: RegisterUser): Handler<RoutingContext> {
    override fun handle(event: RoutingContext) {
        try {
            val user = event.getNewUserRequest()

            if (user.name.isEmpty())
                return event.respondBadRequest("Field '${ User::name.name }' cannot be empty")

            if (user.nickname.isEmpty())
                return event.respondBadRequest("Field '${ User::nickname.name }' cannot be empty")

            registerUser(user.name, user.nickname)

            event.respondOK("User registered successfully")
        }
        catch (ex: ExistingUserException) {
            event
                .response()
                .setStatusCode(HttpResponseStatus.CONFLICT.code())
                .end(ex.message)
        }
        catch(ex: Exception) {
            event.respondInternalServerError(ex.message)
        }
    }
}
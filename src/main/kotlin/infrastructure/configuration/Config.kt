package infrastructure.configuration

data class Config(val serverConfiguration: ServerConfiguration, val twitterServerConfig: TwitterServerConfig)
data class ServerConfiguration(val port: Int)
data class TwitterServerConfig(val debug: Boolean, val type: RepositoryType, val url: String, val credential: Credential)
data class Credential(val user: String, val password: String)

enum class RepositoryType {
    IN_MEMORY,
    MYSQL
}
package infrastructure.repositories.mysql

import domain.Followee
import domain.Followees
import domain.FollowerRepository
import infrastructure.repositories.mysql.tables.FollowerFolloweeTable
import org.jetbrains.exposed.sql.insertIgnore
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class MySqlFollowerRepository: FollowerRepository {
    override fun followUser(followerID: String, followeeID: String) {
        transaction {
            FollowerFolloweeTable.insertIgnore {
                it[FollowerFolloweeTable.followerID] = followerID
                it[FollowerFolloweeTable.followeeID] = followeeID
            }
        }
    }

    override fun getFolloweesByUser(followerID: String): Followees {
        val followees = transaction {
            FollowerFolloweeTable.select {
                FollowerFolloweeTable.followerID eq followerID
            }.map { Followee(it[FollowerFolloweeTable.followeeID]) }
        }

        return Followees(followerID, followees)
    }
}
package infrastructure.repositories.mysql

import domain.Twit
import domain.TwitRepository
import infrastructure.repositories.mysql.tables.TwitTable
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class MySqlTwitRepository: TwitRepository {

    override fun add(twit: Twit) {
        transaction {
            TwitTable.insert {
                it[nickname] = twit.nickname
                it[message] = twit.message
            }
        }
    }

    override fun findByUser(nickname: String): List<Twit> {
        return transaction {
            TwitTable.select { TwitTable.nickname eq nickname }.map { TwitTable.toTwit(it) }
        }
    }

    override fun findByID(id: Int): Twit {
        return transaction {
            TwitTable.select { TwitTable.ID eq id }.map { TwitTable.toTwit(it) }.first()
        }
    }
}
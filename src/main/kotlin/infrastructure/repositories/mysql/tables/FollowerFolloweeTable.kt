package infrastructure.repositories.mysql.tables

import domain.Followee
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

object FollowerFolloweeTable: Table() {
    val followerID: Column<String> = varchar("Follower_ID", 256)
    val followeeID: Column<String> = varchar("Followee_ID", 256)

    override val primaryKey = PrimaryKey(followerID, followeeID, name = "PK_relation_ID")

    fun getFollowees(nickname: String): List<Followee> {
        return transaction {
            select { followerID eq nickname }.map { rowToFollowee(it) }
        }
    }

    private fun rowToFollowee(row: ResultRow): Followee {
        return Followee(row[followeeID])
    }
}
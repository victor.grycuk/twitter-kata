package infrastructure.repositories.mysql.tables

import domain.Twit
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table

object TwitTable: Table() {
    val ID: Column<Int> = TwitTable.integer("ID").autoIncrement()
    val nickname: Column<String> = TwitTable.varchar("nickname", 256)
    val message: Column<String> = TwitTable.varchar("message", 256)

    override val primaryKey = PrimaryKey(ID, name = "PK_twit_ID")

    fun toTwit(row: ResultRow): Twit {
        return Twit(
            nickname = row[nickname],
            message = row[message]
        )
    }
}
package infrastructure.repositories.mysql.tables

import domain.User
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table

object UserTable: Table() {
    val name: Column<String> = varchar("name", 256)
    val nickname: Column<String> = varchar("nickname", 256)

    override val primaryKey = PrimaryKey(nickname, name="PK_User_ID")

    fun toUser(row: ResultRow): User {
        return User(
            nickname = row[nickname],
            name = row[name]
        )
    }
}
package infrastructure.repositories.mysql

import infrastructure.configuration.TwitterServerConfig
import infrastructure.repositories.mysql.tables.FollowerFolloweeTable
import infrastructure.repositories.mysql.tables.TwitTable
import infrastructure.repositories.mysql.tables.UserTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

class DatabaseInitializer(serverConfig: TwitterServerConfig) {
    private val driver = "com.mysql.cj.jdbc.Driver"
    private val url = serverConfig.url
    private val user = serverConfig.credential.user
    private val password = serverConfig.credential.password
    private val isDebug = serverConfig.debug

    fun openConnection() {
        Database.connect(url, driver, user, password)
        transaction {
            if (isDebug) dropTables()
            createTables()
        }
    }

    private fun createTables() {
        SchemaUtils.create(UserTable)
        SchemaUtils.create(TwitTable)
        SchemaUtils.create(FollowerFolloweeTable)
    }

    private fun dropTables() {
        SchemaUtils.drop(UserTable)
        SchemaUtils.drop(TwitTable)
        SchemaUtils.drop(FollowerFolloweeTable)
    }
}
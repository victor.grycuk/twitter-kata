package infrastructure.repositories.mysql

import domain.User
import domain.UserRepository
import infrastructure.repositories.mysql.tables.UserTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class MySqlUserRepository: UserRepository {
    override fun add(user: User) {
        transaction {
            UserTable.insert {
                it[nickname] = user.nickname
                it[name] = user.name
            }
        }
    }

    override fun findByNickname(nickname: String): User? {
        return transaction {
            val result = UserTable.select { UserTable.nickname eq nickname }.map { UserTable.toUser(it) }
            if (result.isNotEmpty()) result.single()
            else null
        }
    }

    override fun checkIfUserExists(nickname: String): Boolean {
        return transaction {
            UserTable.select { UserTable.nickname eq nickname }.map { UserTable.toUser(it) }.isNotEmpty()
        }
    }

    override fun updateUser(user: User) {
        transaction {
            UserTable.update({ UserTable.nickname eq user.nickname }) {
                it[name] = user.name
            }
        }
    }
}
package infrastructure.repositories.inmemory

import domain.User
import domain.UserRepository

class InMemoryUserRepository : UserRepository {
    private val registeredUsers = mutableListOf<User>()

    override fun add(user: User) {
        registeredUsers.add(user)
    }

    override fun findByNickname(nickname: String): User? {
        return registeredUsers.find { u -> u.nickname == nickname }
    }

    override fun checkIfUserExists(nickname: String): Boolean {
        return registeredUsers.find { u -> u.nickname == nickname } != null
    }

    override fun updateUser(user: User) {
        val userToUpdate = registeredUsers.find{ u -> u.nickname == user.nickname } ?: return

        registeredUsers[registeredUsers.indexOf(userToUpdate)] = user
    }
}
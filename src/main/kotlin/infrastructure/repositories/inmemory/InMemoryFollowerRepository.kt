package infrastructure.repositories.inmemory

import domain.Followee
import domain.Followees
import domain.FollowerRepository

class InMemoryFollowerRepository: FollowerRepository {
    private val followers = mutableMapOf<String, String>()

    override fun followUser(followerID: String, followeeID: String) {
        followers[followerID] = followeeID
    }

    override fun getFolloweesByUser(followerID: String): Followees {
        val value = followers.filter { it.key == followerID }.entries.map { Followee(it.value) }
        return Followees(followerID, value)
    }
}
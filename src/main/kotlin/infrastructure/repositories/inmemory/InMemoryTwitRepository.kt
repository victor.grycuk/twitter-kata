package infrastructure.repositories.inmemory

import domain.Twit
import domain.TwitRepository

class InMemoryTwitRepository: TwitRepository {
    private val twits = mutableListOf<Twit>()

    override fun add(twit: Twit) {
        twits.add(twit)
    }

    override fun findByUser(nickname: String): List<Twit> {
        return twits.filter{ it.nickname == nickname }
    }

    override fun findByID(id: Int): Twit {
        return twits[id]
    }
}

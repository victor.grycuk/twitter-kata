package action

import domain.TwitRepository
import domain.UserRepository
import domain.errors.UserNotFoundException

class RetrieveTwitsByUser(private val twitRepository: TwitRepository, private val userRepository: UserRepository) {
    operator fun invoke(nickname: String): List<String> {
        if (!userRepository.checkIfUserExists(nickname))
            throw UserNotFoundException("The user $nickname does not exist")

        return twitRepository.findByUser(nickname).map { it.message }
    }
}
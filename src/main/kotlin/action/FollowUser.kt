package action

import domain.FollowerRepository
import domain.UserRepository
import domain.errors.UserNotFoundException

class FollowUser(private val followerRepository: FollowerRepository, private val userRepository: UserRepository) {
    operator fun invoke(followerNickname: String, followeeNickname: String) {
        validateUser(followerNickname)
        validateUser(followeeNickname)

        followerRepository.followUser(followerNickname, followeeNickname)
    }

    private fun validateUser(nickname: String) {
        if (!userRepository.checkIfUserExists(nickname))
            throw UserNotFoundException("The user $nickname does not exist")
    }
}
package action

import domain.Twit
import domain.TwitRepository
import domain.UserRepository
import domain.errors.UserNotFoundException

class TwitMessage(private val twitRepository: TwitRepository, private val userRepository: UserRepository) {
    operator fun invoke(nickname: String, text: String) {
        val user = userRepository.findByNickname(nickname) ?:
            throw UserNotFoundException("User with nickname '$nickname' not found.")

        twitRepository.add(Twit(user.nickname, text))
    }
}
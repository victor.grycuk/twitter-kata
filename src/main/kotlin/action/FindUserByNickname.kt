package action

import domain.User
import domain.UserRepository
import domain.errors.UserNotFoundException

class FindUserByNickname(private val userRepository: UserRepository) {
    operator fun invoke(nickname: String): User {
        return userRepository.findByNickname(nickname)
            ?: throw UserNotFoundException("The user $nickname does not exist")
    }
}
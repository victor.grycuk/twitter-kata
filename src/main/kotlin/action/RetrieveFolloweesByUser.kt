package action

import domain.Followees
import domain.FollowerRepository
import domain.UserRepository
import domain.errors.UserNotFoundException

class RetrieveFolloweesByUser(private val followerRepository: FollowerRepository, private val userRepository: UserRepository) {
    operator fun invoke(nickname: String): Followees {
        if (!userRepository.checkIfUserExists(nickname))
            throw UserNotFoundException("The user '$nickname' was not found")

        return followerRepository.getFolloweesByUser(nickname)
    }
}
package action

import domain.User
import domain.UserRepository
import domain.errors.ExistingUserException

class RegisterUser(private val userRepository: UserRepository) {
    operator fun invoke(name: String, nickname: String) {
        if (userRepository.checkIfUserExists(nickname))
            throw ExistingUserException("Nickname is already in use")

        userRepository.add(User(name, nickname))
    }
}
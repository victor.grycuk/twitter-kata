package action

import domain.UserRepository
import domain.errors.UserNotFoundException

class UpdateUser(private val userRepository: UserRepository) {
    operator fun invoke(nickname: String, newName: String) {
        val user = userRepository.findByNickname(nickname)
            ?: throw UserNotFoundException("The user $nickname does not exist")

        userRepository.updateUser(user.copy(name = newName))
    }
}
package handlers.services

import domain.User
import infrastructure.configuration.ServerConfiguration
import infrastructure.server.Endpoints
import infrastructure.server.HandlerDI
import infrastructure.server.VertxServer
import infrastructure.server.handlers.FollowUserHandler
import infrastructure.server.handlers.TwitMessageHandler
import infrastructure.server.handlers.UpdateUserHandler
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientRequest
import io.vertx.core.http.HttpClientResponse
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import java.util.concurrent.TimeUnit

@ExtendWith(VertxExtension::class)
class VertxShould {
    private lateinit var server: VertxServer
    private lateinit var httpClient: HttpClient
    private lateinit var testContext: VertxTestContext
    private val serverConfiguration: ServerConfiguration = mock()
    private val handlerDI: HandlerDI = mock()
    private val handlerMock: Handler<RoutingContext> = mock()
    private val user = User("Jack", "@jack")
    private val userUserForm = UpdateUserHandler.UpdateRequest("@jack", "Juan")
    private val followUserForm = FollowUserHandler.FollowRequest("@jack", "@alice")
    private val twitMessageForm = TwitMessageHandler.TwitRequest("@jack", "test message")

    @Before
    internal fun init() {
        setupMocks()
        setupServer()
        server = VertxServer(serverConfiguration, handlerDI)
    }

    @After
    internal fun close() {
        server.close()
    }

    @Test
    fun `call the register user action when the register user endpoint is called`() {
        `given a running vertx server`()

        `when the endpoint is called`(HttpMethod.POST, Endpoints.registerUser, Json.encodePrettily(user))

        `then the handler should have been called`()
    }

    @Test
    fun `call the find user action when the find user endpoint is called`() {
        `given a running vertx server`()

        `when the endpoint is called`(HttpMethod.GET, Endpoints.findUser.replace(":nickname", user.nickname), "")

        `then the handler should have been called`()
    }

    @Test
    fun `call the update user action when the update user endpoint is called`() {
        `given a running vertx server`()

        `when the endpoint is called`(HttpMethod.PUT, Endpoints.updateUser, Json.encodePrettily(userUserForm))

        `then the handler should have been called`()
    }

    @Test
    fun `call the follow user action when the follow user endpoint is called`() {
        `given a running vertx server`()

        `when the endpoint is called`(HttpMethod.PUT, Endpoints.followUser, Json.encodePrettily(followUserForm))

        `then the handler should have been called`()
    }

    @Test
    fun `call the retrieve followees action when the retrieve followees endpoint is called`() {
        `given a running vertx server`()

        `when the endpoint is called`(HttpMethod.GET, Endpoints.getFollowees.replace(":nickname", user.nickname), "")

        `then the handler should have been called`()
    }

    @Test
    fun `call the twit message action when the twit message endpoint is called`() {
        `given a running vertx server`()

        `when the endpoint is called`(HttpMethod.POST, Endpoints.twitMessage, Json.encodePrettily(twitMessageForm))

        `then the handler should have been called`()
    }

    @Test
    fun `call the retrieve twits action when the retrieve twits endpoint is called`() {
        `given a running vertx server`()

        `when the endpoint is called`(HttpMethod.GET, Endpoints.getTwits.replace(":nickname", user.nickname), "")

        `then the handler should have been called`()
    }

    private fun `given a running vertx server`() {
        server.start()
    }

    private fun `then the handler should have been called`() {
        testContext.awaitCompletion(5, TimeUnit.SECONDS)
        verify(handlerMock, times(1)).handle(any())
    }

    private fun `when the endpoint is called`(method: HttpMethod, endpoint: String, body: String) {
        httpClient.request(method, 8080, "localhost", endpoint)
            .compose { req: HttpClientRequest ->
                req.send(body).compose { obj: HttpClientResponse -> obj.body() }
            }
            .onComplete(testContext.succeeding {
                testContext.verify {
                    testContext.completeNow()
                }
            })
    }

    private fun setupMocks() {
        Mockito.`when`(serverConfiguration.port).thenReturn(8080)

        Mockito.`when`(handlerDI.getRegisterHandler()).thenReturn(handlerMock)
        Mockito.`when`(handlerDI.getFindHandler()).thenReturn(handlerMock)
        Mockito.`when`(handlerDI.getUpdateHandler()).thenReturn(handlerMock)
        Mockito.`when`(handlerDI.getFollowHandler()).thenReturn(handlerMock)
        Mockito.`when`(handlerDI.getGetFolloweesHandler()).thenReturn(handlerMock)
        Mockito.`when`(handlerDI.getTwitHandler()).thenReturn(handlerMock)
        Mockito.`when`(handlerDI.getGetTwitsHandler()).thenReturn(handlerMock)
    }

    private fun setupServer() {
        val vertx: Vertx = Vertx.vertx()
        httpClient = vertx.createHttpClient()
        testContext = VertxTestContext()
    }
}
package handlers

import org.mockito.ArgumentCaptor

inline fun <reified T> ArgumentCaptor<T>.captureNotNull(): T = this.capture()
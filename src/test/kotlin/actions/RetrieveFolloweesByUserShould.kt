package handlers.actions

import action.RetrieveFolloweesByUser
import domain.*
import domain.errors.UserNotFoundException
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock
import org.assertj.core.api.Assertions.assertThat
import org.mockito.ArgumentMatchers.anyString

class RetrieveFolloweesByUserShould {
    lateinit var retrieveFollowees: RetrieveFolloweesByUser
    lateinit var followees: Followees
    private val followeeRepository: FollowerRepository = mock()
    private val userRepository: UserRepository = mock()
    private val user = User("Jack", "@jack")

    @Test
    fun `return a list of followees of a given user`() {
        `given a repository with an user with followees`()

        `when the action is given the user nickname to retrieve followees`(user.nickname)

        `then it should have returned a correct instance of Followees`()
    }

    @Test(expected = UserNotFoundException::class)
    fun `throw an exception if the user was not found`() {
        `given a repository with with no users`()

        `when the action checks if the user exists it should throw UserNotFoundException`(user.nickname)
    }

    private fun `given a repository with an user with followees`() {
        setupUserRepositoryWithValidUser()
        setupFolloweeRepositoryWithValidUser()

        retrieveFollowees = RetrieveFolloweesByUser(followeeRepository, userRepository)
    }

    private fun `given a repository with with no users`() {
        setupUserRepositoryWithNotFoundUser()
    }

    private fun `when the action is given the user nickname to retrieve followees`(nickname: String) {
        followees = retrieveFollowees.invoke(nickname)
    }

    private fun `when the action checks if the user exists it should throw UserNotFoundException`(nickname: String) {
        followees = retrieveFollowees.invoke(nickname)
    }

    private fun `then it should have returned a correct instance of Followees`() {
        assertThat(followees.follower).isEqualTo(user.nickname)
        assertThat(followees.followees[0].nickname).isEqualTo("followee 1")
        assertThat(followees.followees[1].nickname).isEqualTo("followee 2")
    }

    private fun setupUserRepositoryWithValidUser() {
        Mockito.`when`(userRepository.checkIfUserExists(user.nickname)).thenReturn(true)
    }

    private fun setupUserRepositoryWithNotFoundUser() {
        Mockito.`when`(userRepository.checkIfUserExists(anyString())).thenReturn(false)
    }

    private fun setupFolloweeRepositoryWithValidUser() {
        val followees = Followees(user.nickname, listOf(Followee("followee 1"), Followee("followee 2")))
        Mockito.`when`(followeeRepository.getFolloweesByUser(user.nickname)).thenReturn(followees)
    }
}
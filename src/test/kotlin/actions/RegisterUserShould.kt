package actions

import action.RegisterUser
import domain.User
import domain.UserRepository
import domain.errors.ExistingUserException
import infrastructure.repositories.inmemory.InMemoryUserRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class RegisterUserShould {
    private lateinit var registerUser: RegisterUser
    private lateinit var userRepository: UserRepository
    private val userJack = User("Jack Bauer", "@jack")

    @Test
    fun `create new user`() {
        `given an user repository`(InMemoryUserRepository())
        `given a RegisterUser action`()

        `when a given user is registered`(userJack)

        `then the user should exist in the repository`()
    }

    @Test(expected = ExistingUserException::class)
    fun `not allow duplicate nicknames`() {
        `given an user repository with a registered user`(InMemoryUserRepository(), userJack)
        `given a RegisterUser action`()

        `when a given user is registered`(userJack)
    }
    private fun `given an user repository`(repository: UserRepository) {
        userRepository = repository
    }

    private fun `given a RegisterUser action`() {
        registerUser = RegisterUser(userRepository)
    }

    private fun `given an user repository with a registered user`(repository: UserRepository, user: User) {
        userRepository = repository
        userRepository.add(user)
    }

    private fun `when a given user is registered`(user: User){
        registerUser(user.name, user.nickname)
    }

    private fun `then the user should exist in the repository`() {
        val newUser = userRepository.findByNickname(userJack.nickname)
        assertThat(newUser).isEqualTo(userJack)
    }
}
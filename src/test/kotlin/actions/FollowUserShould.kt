package actions

import action.FollowUser
import domain.FollowerRepository
import domain.User
import domain.UserRepository
import infrastructure.repositories.inmemory.InMemoryFollowerRepository
import infrastructure.repositories.inmemory.InMemoryUserRepository
import org.junit.Test
import kotlin.test.assertTrue

class FollowUserShould {
    private lateinit var followUser: FollowUser
    private lateinit var userRepository: UserRepository
    private lateinit var followerRepository: FollowerRepository

    private val userJack = User("Jack Bauer", "@jack")
    private val userAlice = User("Alice", "@alice")

    @Test
    fun `return a list of users that a given user is following`() {
        `given an user repository with two users`(InMemoryUserRepository())
        `given a follower repository`(InMemoryFollowerRepository())
        `given a FollowUser action`()

        `when an user follows another`()

        `then the first should have the second as followee`()
    }

    private fun `given an user repository with two users`(repository: UserRepository) {
        userRepository = repository
        userRepository.add(userJack)
        userRepository.add(userAlice)
    }

    private fun `given a follower repository`(repository: FollowerRepository) {
        followerRepository = repository
    }

    private fun `given a FollowUser action`() {
        followUser = FollowUser(followerRepository, userRepository)
    }

    private fun `when an user follows another`() {
        followUser(userJack.nickname, userAlice.nickname)
    }

    private fun `then the first should have the second as followee`() {
        val follower = followerRepository.getFolloweesByUser(userJack.nickname)
        assertTrue(follower.followees.any{u -> u.nickname == userAlice.nickname})
    }
}
package actions

import action.TwitMessage
import domain.TwitRepository
import domain.User
import domain.UserRepository
import infrastructure.repositories.inmemory.InMemoryTwitRepository
import infrastructure.repositories.inmemory.InMemoryUserRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class TwitMessageShould {
    private lateinit var twitterRepository: TwitRepository
    private lateinit var userRepository: UserRepository
    private lateinit var twitAction: TwitMessage

    private val userJack = User("Jack Bauer", "@jack")

    @Test
    fun `persist twitted messages on a repository`() {
        `given a twit repository`(InMemoryTwitRepository())
        `given an user repository with a registered user`(InMemoryUserRepository())
        `given a Twit action`()

        `when an user twits`(userJack, "test")

        `the twit should exist in the repository`(userJack, "test")
    }

    private fun `given a twit repository`(inMemoryTwitRepository: TwitRepository) {
        twitterRepository = inMemoryTwitRepository
    }

    private fun `given an user repository with a registered user`(userRepository: UserRepository) {
        this.userRepository = userRepository
        this.userRepository.add(userJack)
    }

    private fun `given a Twit action`() {
        twitAction = TwitMessage(twitterRepository, userRepository)
    }

    private fun `when an user twits`(user: User, text: String) {
        twitAction(user.nickname, text)
    }

    private fun `the twit should exist in the repository`(user: User, text: String) {
        val twit = twitterRepository.findByUser(user.nickname).first()
        assertThat(twit.message).isEqualTo(text)
    }
}
package actions

import action.RetrieveTwitsByUser
import domain.Twit
import domain.TwitRepository
import domain.User
import domain.UserRepository
import infrastructure.repositories.inmemory.InMemoryTwitRepository
import infrastructure.repositories.inmemory.InMemoryUserRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class RetrieveTwitsShould {
    lateinit var twitterRepository: TwitRepository
    lateinit var retrieveTwitsAction: RetrieveTwitsByUser
    lateinit var userRepository: UserRepository

    private val userJack = User("Jack Bauer", "@jack")

    @Test
    fun `return a list of messages for a given user`() {
        `given an in memory twit repository with several messages`(InMemoryTwitRepository())
        `given an in memory user repository`(InMemoryUserRepository())
        `given a Twit action`()

        val messages = `when the list of messages for a given user is retrieved`(userJack)

        `then it should contain all the messages of a given user`(messages)
    }

    private fun `given an in memory twit repository with several messages`(inMemoryTwitRepository: TwitRepository) {
        twitterRepository = inMemoryTwitRepository
        twitterRepository.add(Twit(userJack.nickname, "Jack message 1"))
        twitterRepository.add(Twit(userJack.nickname, "Jack message 2"))
        twitterRepository.add(Twit("@Alice", "Alice message 1"))
        twitterRepository.add(Twit(userJack.nickname, "Jack message 3"))
    }

    private fun `given an in memory user repository`(repository: UserRepository) {
        userRepository = repository
        userRepository.add(userJack)
    }

    private fun `given a Twit action`() {
        retrieveTwitsAction = RetrieveTwitsByUser(twitterRepository, userRepository)
    }

    private fun `when the list of messages for a given user is retrieved`(user: User): List<String> {
        return retrieveTwitsAction(user.nickname)
    }

    private fun `then it should contain all the messages of a given user`(messages: List<String>) {
        assertThat(messages[0]).isEqualTo("Jack message 1")
        assertThat(messages[1]).isEqualTo("Jack message 2")
        assertThat(messages[2]).isEqualTo("Jack message 3")
    }
}
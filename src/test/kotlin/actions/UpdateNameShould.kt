package actions

import action.UpdateUser
import domain.User
import domain.UserRepository
import infrastructure.repositories.inmemory.InMemoryUserRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class UpdateNameShould {
    private lateinit var userRepository: UserRepository
    private lateinit var updateUser: UpdateUser

    private val userJack = User("Jack Bauer", "@jack")

    @Test
    fun `allow users to update their name`() {
        val newName = "Juan Bauer"

        `given an user repository with a registered user`(InMemoryUserRepository())
        `given an UpdateUser action`()

        `when the user updates its name`(newName)

        `then the changes should be reflected in the repository`(newName)
    }

    private fun `given an user repository with a registered user`(repository: UserRepository) {
        userRepository = repository
        userRepository.add(userJack.copy(name = "Juan Bauer"))
    }

    private fun `given an UpdateUser action`() {
        updateUser = UpdateUser(userRepository)
    }

    private fun `when the user updates its name`(newName: String) {
        updateUser(userJack.nickname, newName)
    }

    private fun `then the changes should be reflected in the repository`(newName: String) {
        val updatedUser = userRepository.findByNickname(userJack.nickname)!!
        assertThat(updatedUser.name).isEqualTo(newName)
    }
}
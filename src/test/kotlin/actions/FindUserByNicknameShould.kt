package handlers.actions

import action.FindUserByNickname
import domain.User
import domain.UserRepository
import domain.errors.UserNotFoundException
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.kotlin.mock

class FindUserByNicknameShould {
    private lateinit var findUserAction: FindUserByNickname
    private lateinit var userFound: User
    private val repository: UserRepository = mock()
    private val user = User("Jack", "@jack")

    @Test
    fun `return an user when given a valid nickname`() {
        `given an user repository with a stored user`()

        `when the action is invoked`()

        `then it should have returned a valid user`()
    }

    @Test(expected = UserNotFoundException::class)
    fun `throw an exception when he user is not found`() {
        `given an user repository with a missing user`()

        `when the action is invoked it should throw UserNotFoundException`()
    }

    private fun `given an user repository with a stored user`() {
        setupUserRepositoryMockWithAValidUser()
        findUserAction = FindUserByNickname(repository)
    }

    private fun `given an user repository with a missing user`() {
        setupUserRepositoryMockWithAMissingUser()
        findUserAction = FindUserByNickname(repository)
    }

    private fun `when the action is invoked`() {
        userFound = findUserAction.invoke(user.nickname)
    }

    private fun `when the action is invoked it should throw UserNotFoundException`() {
        findUserAction.invoke(user.nickname)
    }

    private fun `then it should have returned a valid user`() {
        assertThat(userFound).isEqualTo(user)
    }

    private fun setupUserRepositoryMockWithAValidUser() {
        Mockito.`when`(repository.findByNickname(user.nickname)).thenReturn(user)
    }

    private fun setupUserRepositoryMockWithAMissingUser() {
        Mockito.`when`(repository.findByNickname(anyString())).thenReturn(null)
    }
}
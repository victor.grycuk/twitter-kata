package handlers

import action.RetrieveFolloweesByUser
import domain.Followee
import domain.Followees
import domain.errors.UserNotFoundException
import infrastructure.server.Parameter
import infrastructure.server.handlers.RetrieveFolloweesHandler
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.`when`
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class RetrieveFolloweesHandlerShould {
    private lateinit var statusCaptor: ArgumentCaptor<Int>
    private lateinit var messageCaptor: ArgumentCaptor<String>
    private lateinit var retrieveFolloweesNicknameCaptor: ArgumentCaptor<String>
    private lateinit var actionMock: RetrieveFolloweesByUser
    private lateinit var routingContextMock: RoutingContext
    private lateinit var responseMock: HttpServerResponse
    private lateinit var followees: Followees

    @Before
    fun initialize() {
        statusCaptor = ArgumentCaptor.forClass(Int::class.java)
        messageCaptor = ArgumentCaptor.forClass(String::class.java)
        retrieveFolloweesNicknameCaptor = ArgumentCaptor.forClass(String::class.java)
        actionMock = mock()
        routingContextMock = mock()
        responseMock = mock()
        val followeesList = listOf(Followee("@jane"), Followee("@vic"))
        followees = Followees("jack", followeesList)

        setupMockRoutingContext()
        setupMockResponse()
    }

    @Test
    fun `return status 200 with a list of followees given a valid user`() {
        `given a valid user to retrieve its followees`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.OK.code(), Json.encodePrettily(followees))
    }

    @Test
    fun `return status 400 when receiving an empty nickname parameter`() {
        `given an empty nickname parameter`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Parameter '${ Parameter.NICKNAME }' cannot be empty.")
    }

    @Test
    fun `return status 404 when trying to retrieve followees of a non existent user`() {
        `given a non existing user to retrieve followees`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.NOT_FOUND.code(), "The user ${ followees.follower } does not exist")
    }

    @Test
    fun `pass the correct values to the action`() {
        `given a valid user to retrieve its followees`()

        `when the handle is invoked`()

        `then the action should have received the correct parameters`("@jack")
    }

    private fun `given a valid user to retrieve its followees`() {
        setupParameterMock(Parameter.NICKNAME, "@jack")
        setupRetrieveFolloweesByUserMock()
    }

    private fun `given an empty nickname parameter`() {
        setupParameterMock(Parameter.NICKNAME, "")
    }

    private fun `given a non existing user to retrieve followees`() {
        setupParameterMock(Parameter.NICKNAME, "@jack")
        setupRetrieveFolloweesMockWithNotFoundException()
    }

    private fun `when the handle is invoked`() {
        RetrieveFolloweesHandler(actionMock).handle(routingContextMock)
    }

    private fun `then it should respond with`(statusCode: Int, responseMessage: String) {
        assertThat(statusCaptor.value).isEqualTo(statusCode)
        assertThat(messageCaptor.value).isEqualTo(responseMessage)
    }

    private fun `then the action should have received the correct parameters`(value: String) {
        assertThat(retrieveFolloweesNicknameCaptor.value).isEqualTo(value)
    }

    private fun setupParameterMock(parameterName: String, parameterValue: String) {
        `when`(routingContextMock.request()).thenReturn(mock())
        `when`(routingContextMock.request().getParam(parameterName)).thenReturn(parameterValue)
    }

    private fun setupRetrieveFolloweesByUserMock() {
        `when`(actionMock.invoke(retrieveFolloweesNicknameCaptor.captureNotNull())).thenReturn(followees)
    }

    private fun setupMockRoutingContext() {
        `when`(routingContextMock.body).thenReturn(Buffer.buffer(""))
        `when`(routingContextMock.response()).thenReturn(responseMock)
    }

    private fun setupMockResponse() {
        `when`(responseMock.setStatusCode(statusCaptor.capture())).thenReturn(responseMock)
        `when`(responseMock.putHeader("content-type", "application/json")).thenReturn(responseMock)
        `when`(responseMock.end(messageCaptor.capture())).doReturn(Future.succeededFuture())
    }

    private fun setupRetrieveFolloweesMockWithNotFoundException() {
        `when`(actionMock.invoke(ArgumentMatchers.anyString()))
            .doAnswer { throw UserNotFoundException("The user ${ followees.follower } does not exist") }
    }
}
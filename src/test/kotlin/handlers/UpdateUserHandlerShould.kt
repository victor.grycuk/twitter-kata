package handlers

import action.UpdateUser
import domain.errors.UserNotFoundException
import infrastructure.server.handlers.UpdateUserHandler
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.`when`
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class UpdateUserHandlerShould {
    private lateinit var statusCaptor: ArgumentCaptor<Int>
    private lateinit var messageCaptor: ArgumentCaptor<String>
    private lateinit var updateUserNicknameCaptor: ArgumentCaptor<String>
    private lateinit var updateUserNewNameCaptor: ArgumentCaptor<String>
    private lateinit var actionMock: UpdateUser
    private lateinit var routingContextMock: RoutingContext
    private lateinit var responseMock: HttpServerResponse
    private lateinit var user: UpdateUserHandler.UpdateRequest

    @Before
    fun initialize() {
        statusCaptor = ArgumentCaptor.forClass(Int::class.java)
        messageCaptor = ArgumentCaptor.forClass(String::class.java)
        updateUserNicknameCaptor = ArgumentCaptor.forClass(String::class.java)
        updateUserNewNameCaptor = ArgumentCaptor.forClass(String::class.java)
        actionMock = mock()
        routingContextMock = mock()
        responseMock = mock()
        user = UpdateUserHandler.UpdateRequest("@jack", "juan")
    }

    @Test
    fun `return status 200 after successfully updating a user name`() {
        `given a valid user to update`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.OK.code(), "User updated successfully")
    }

    @Test
    fun `return status 404 when trying to update a non existent user`() {
        `given a non existing user to update`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.NOT_FOUND.code(), "The user ${ user.nickname } does not exist")
    }

    @Test
    fun `return status 400 when trying to update an empty name`() {
        `given an update request with an empty new name`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Parameter '${ UpdateUserHandler.UpdateRequest::newName.name }' cannot be empty.")
    }

    @Test
    fun `pass the correct values to the action`() {
        `given a valid user to update`()
        `given the parameters`()

        `when the handle is invoked`()

        `then the action should have received the correct parameters`(user.nickname, user.newName)
    }

    private fun `given a valid user to update`() {
        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given a non existing user to update`() {
        setupUpdateUserMockWithNotFoundException()
        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given an update request with an empty new name`() {
        user = user.copy(newName = "")
        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given the parameters`() {
        doNothing().`when`(actionMock).invoke(updateUserNicknameCaptor.captureNotNull(), updateUserNewNameCaptor.captureNotNull())
    }

    private fun `when the handle is invoked`() {
        UpdateUserHandler(actionMock).handle(routingContextMock)
    }

    private fun `then it should respond with`(statusCode: Int, responseMessage: String) {
        assertThat(statusCaptor.value).isEqualTo(statusCode)
        assertThat(messageCaptor.value).isEqualTo(responseMessage)
    }

    private fun `then the action should have received the correct parameters`(value1: String, value2: String) {
        assertThat(updateUserNicknameCaptor.value).isEqualTo(value1)
        assertThat(updateUserNewNameCaptor.value).isEqualTo(value2)
    }

    private fun setupMockRoutingContext() {
        `when`(routingContextMock.body).thenReturn(Buffer.buffer(Json.encodePrettily(user)))
        `when`(routingContextMock.response()).thenReturn(responseMock)
    }

    private fun setupMockResponse() {
        `when`(responseMock.setStatusCode(statusCaptor.capture())).thenReturn(responseMock)
        `when`(responseMock.end(messageCaptor.capture())).doReturn(Future.succeededFuture())
    }

    private fun setupUpdateUserMockWithNotFoundException() {
        `when`(actionMock.invoke(anyString(), anyString()))
            .doAnswer { throw UserNotFoundException("The user ${ user.nickname } does not exist") }
    }
}
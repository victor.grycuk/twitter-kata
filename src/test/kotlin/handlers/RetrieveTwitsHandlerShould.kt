package handlers

import action.RetrieveTwitsByUser
import domain.errors.UserNotFoundException
import infrastructure.server.Parameter
import infrastructure.server.handlers.RetrieveTwitsHandler
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.`when`
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class RetrieveTwitsHandlerShould {
    private lateinit var statusCaptor: ArgumentCaptor<Int>
    private lateinit var messageCaptor: ArgumentCaptor<String>
    private lateinit var retrieveTwitsNicknameCaptor: ArgumentCaptor<String>
    private lateinit var actionMock: RetrieveTwitsByUser
    private lateinit var routingContextMock: RoutingContext
    private lateinit var responseMock: HttpServerResponse
    private lateinit var twitList: List<String>

    @Before
    fun initialize() {
        statusCaptor = ArgumentCaptor.forClass(Int::class.java)
        messageCaptor = ArgumentCaptor.forClass(String::class.java)
        retrieveTwitsNicknameCaptor = ArgumentCaptor.forClass(String::class.java)
        actionMock = mock()
        routingContextMock = mock()
        responseMock = mock()
        twitList = listOf("@jane", "@vic")

        setupRetrieveTwitsByUserMock()
        setupMockRoutingContext()
        setupMockResponse()
    }

    @Test
    fun `return status 200 after successfully retrieving twit by an user`() {
        `given a valid user to retrieve its twits`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.OK.code(), Json.encodePrettily(twitList))
    }

    @Test
    fun `return status 400 when receiving an empty nickname parameter`() {
        `given an empty nickname parameter`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Parameter '${ Parameter.NICKNAME }' cannot be empty.")
    }

    @Test
    fun `return status 404 when the user is not found`() {
        `given a non existing user to retrieve twits`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.NOT_FOUND.code(), "The user @jack does not exist")
    }

    @Test
    fun `pass the correct values to the action`() {
        `given a valid user to retrieve its twits`()

        `when the handle is invoked`()

        `then the action should have received the correct parameters`("@jack")
    }

    private fun `given a valid user to retrieve its twits`() {
        setupParameterMock(Parameter.NICKNAME, "@jack")
    }

    private fun `given an empty nickname parameter`() {
        setupParameterMock(Parameter.NICKNAME, "")
    }

    private fun `given a non existing user to retrieve twits`() {
        setupParameterMock(Parameter.NICKNAME, "@jack")
        setupRetrieveTwitsByUserMockWithNotFoundException()
    }

    private fun `when the handle is invoked`() {
        RetrieveTwitsHandler(actionMock).handle(routingContextMock)
    }

    private fun `then it should respond with`(statusCode: Int, responseMessage: String) {
        assertThat(statusCaptor.value).isEqualTo(statusCode)
        assertThat(messageCaptor.value).isEqualTo(responseMessage)
    }

    private fun `then the action should have received the correct parameters`(value: String) {
        assertThat(retrieveTwitsNicknameCaptor.value).isEqualTo(value)
    }

    private fun setupParameterMock(parameterName: String, parameterValue: String) {
        `when`(routingContextMock.request()).thenReturn(mock())
        `when`(routingContextMock.request().getParam(parameterName)).thenReturn(parameterValue)
    }

    private fun setupRetrieveTwitsByUserMock() {
        `when`(actionMock.invoke(retrieveTwitsNicknameCaptor.captureNotNull())).thenReturn(twitList)
    }

    private fun setupRetrieveTwitsByUserMockWithNotFoundException() {
        `when`(actionMock.invoke(ArgumentMatchers.anyString()))
            .doAnswer { throw UserNotFoundException("The user @jack does not exist") }
    }

    private fun setupMockRoutingContext() {
        `when`(routingContextMock.body).thenReturn(Buffer.buffer(""))
        `when`(routingContextMock.response()).thenReturn(responseMock)
    }

    private fun setupMockResponse() {
        `when`(responseMock.setStatusCode(statusCaptor.capture())).thenReturn(responseMock)
        `when`(responseMock.putHeader("content-type", "application/json")).thenReturn(responseMock)
        `when`(responseMock.end(messageCaptor.capture())).doReturn(Future.succeededFuture())
    }
}
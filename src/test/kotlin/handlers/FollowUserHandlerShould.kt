package handlers

import action.FollowUser
import domain.errors.UserNotFoundException
import infrastructure.server.handlers.FollowUserHandler
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.`when`
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class FollowUserHandlerShould {
    private lateinit var statusCaptor: ArgumentCaptor<Int>
    private lateinit var messageCaptor: ArgumentCaptor<String>
    private lateinit var followUserNicknameCaptor: ArgumentCaptor<String>
    private lateinit var followUserFolloweeCaptor: ArgumentCaptor<String>
    private lateinit var actionMock: FollowUser
    private lateinit var routingContextMock: RoutingContext
    private lateinit var responseMock: HttpServerResponse
    private lateinit var body: String
    private lateinit var followRequest: FollowUserHandler.FollowRequest

    @Before
    fun initialize() {
        statusCaptor = ArgumentCaptor.forClass(Int::class.java)
        messageCaptor = ArgumentCaptor.forClass(String::class.java)
        followUserNicknameCaptor = ArgumentCaptor.forClass(String::class.java)
        followUserFolloweeCaptor = ArgumentCaptor.forClass(String::class.java)
        actionMock = mock()
        routingContextMock = mock()
        responseMock = mock()
        body = ""
        followRequest = FollowUserHandler.FollowRequest("@jack", "@jane")
    }

    @Test
    fun `return status 200 after successfully following an user`() {
        `given a valid follow request`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.OK.code(), "User '${ followRequest.nickname }' now is following user '${ followRequest.followee }'.")
    }

    @Test
    fun `return status 400 when the follower field is empty`() {
        `given a follow request with an empty follower nickname`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Field '${ followRequest::nickname.name }' cannot be empty.")
    }

    @Test
    fun `return status 400 when the followee field is empty`() {
        `given a follow request with an empty followee nickname`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Field '${ followRequest::followee.name }' cannot be empty.")
    }

    @Test
    fun `return status 404 when one of the user is not found`() {
        `given a follow request with a missing user`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.NOT_FOUND.code(), "The user ${ followRequest.nickname } does not exist")
    }

    @Test
    fun `pass the correct values to the action`() {
        `given a valid follow request`()
        `given the parameters`()

        `when the handle is invoked`()

        `then the action should have received the correct parameters`(followRequest.nickname, followRequest.followee)
    }

    private fun `given a valid follow request`() {
        body = Json.encodePrettily(followRequest)
        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given a follow request with a missing user`() {
        setupFollowUserMockWithNotFoundException()
        body = Json.encodePrettily(followRequest)
        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given a follow request with an empty follower nickname`() {
        body = Json.encodePrettily(followRequest.copy(nickname = ""))
        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given a follow request with an empty followee nickname`() {
        body = Json.encodePrettily(followRequest.copy(followee = ""))
        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given the parameters`() {
        doNothing().`when`(actionMock).invoke(followUserNicknameCaptor.captureNotNull(), followUserFolloweeCaptor.captureNotNull())
    }

    private fun `when the handle is invoked`() {
        FollowUserHandler(actionMock).handle(routingContextMock)
    }

    private fun `then it should respond with`(statusCode: Int, responseMessage: String) {
        assertThat(statusCaptor.value).isEqualTo(statusCode)
        assertThat(messageCaptor.value).isEqualTo(responseMessage)
    }

    private fun `then the action should have received the correct parameters`(value1: String, value2: String) {
        assertThat(followUserNicknameCaptor.value).isEqualTo(value1)
        assertThat(followUserFolloweeCaptor.value).isEqualTo(value2)
    }

    private fun setupMockRoutingContext() {
        `when`(routingContextMock.body).thenReturn(Buffer.buffer(body))
        `when`(routingContextMock.response()).thenReturn(responseMock)
    }

    private fun setupMockResponse() {
        `when`(responseMock.setStatusCode(statusCaptor.capture())).thenReturn(responseMock)
        `when`(responseMock.end(messageCaptor.capture())).doReturn(Future.succeededFuture())
    }

    private fun setupFollowUserMockWithNotFoundException() {
        `when`(actionMock.invoke(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()))
            .doAnswer { throw UserNotFoundException("The user ${ followRequest.nickname } does not exist") }
    }
}
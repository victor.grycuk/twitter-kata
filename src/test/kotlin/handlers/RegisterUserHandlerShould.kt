package handlers

import action.RegisterUser
import domain.User
import domain.errors.ExistingUserException
import infrastructure.server.handlers.RegisterUserHandler
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Future.succeededFuture
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyString
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock


class RegisterUserHandlerShould {

    private lateinit var statusCaptor: ArgumentCaptor<Int>
    private lateinit var messageCaptor: ArgumentCaptor<String>
    private lateinit var registerUserNameCaptor: ArgumentCaptor<String>
    private lateinit var registerUserNicknameCaptor: ArgumentCaptor<String>
    private lateinit var actionMock: RegisterUser
    private lateinit var routingContextMock: RoutingContext
    private lateinit var responseMock: HttpServerResponse
    private lateinit var requestBody: String
    private lateinit var user: User

    @Before
    fun initialize() {
        statusCaptor = ArgumentCaptor.forClass(Int::class.java)
        messageCaptor = ArgumentCaptor.forClass(String::class.java)
        registerUserNameCaptor = ArgumentCaptor.forClass(String::class.java)
        registerUserNicknameCaptor = ArgumentCaptor.forClass(String::class.java)
        actionMock = mock()
        routingContextMock = mock()
        responseMock = mock()
        user = User("jack", "@jack")
    }

    @Test
    fun `return status 200 after an user is registered`() {
        `given a valid new user request`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.OK.code(), "User registered successfully")
    }

    @Test
    fun `return status 409 after trying to register an existing user`() {
        `given a user repository that throws an existing user exception`()
        `given a valid new user request`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.CONFLICT.code(), "Nickname is already in use")
    }

    @Test
    fun `return status 400 after receiving an empty name field`() {
        `given an user request with a missing name`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Field '${ User::name.name }' cannot be empty")
    }

    @Test
    fun `return status 400 after receiving an empty nickname field`() {
        `given an user request with a missing nickname`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Field '${ User::nickname.name }' cannot be empty")
    }

    @Test
    fun `pass the correct values to the action`() {
        `given a valid new user request`()
        `given the parameters`()

        `when the handle is invoked`()

        `then the action should have received the correct parameters`(user.name, user.nickname)
    }

    private fun `given a valid new user request`() {
        requestBody = Json.encodePrettily(user)

        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given a user repository that throws an existing user exception`() {
        `when`(actionMock.invoke(anyString(), anyString()))
            .doAnswer{ throw ExistingUserException("Nickname is already in use") }
    }

    private fun `given an user request with a missing name`() {
        requestBody = Json.encodePrettily(user.copy(name = ""))

        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given an user request with a missing nickname`() {
        requestBody = Json.encodePrettily(user.copy(nickname = ""))

        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given the parameters`() {
        doNothing().`when`(actionMock).invoke(registerUserNameCaptor.captureNotNull(), registerUserNicknameCaptor.captureNotNull())
    }

    private fun `when the handle is invoked`() {
        RegisterUserHandler(actionMock).handle(routingContextMock)
    }

    private fun `then it should respond with`(statusCode: Int, responseMessage: String) {
        assertThat(statusCaptor.value).isEqualTo(statusCode)
        assertThat(messageCaptor.value).isEqualTo(responseMessage)
    }

    private fun `then the action should have received the correct parameters`(value1: String, value2: String) {
        assertThat(registerUserNameCaptor.value).isEqualTo(value1)
        assertThat(registerUserNicknameCaptor.value).isEqualTo(value2)
    }

    private fun setupMockRoutingContext() {
        `when`(routingContextMock.body).thenReturn(Buffer.buffer(requestBody))
        `when`(routingContextMock.response()).thenReturn(responseMock)
    }

    private fun setupMockResponse() {
        `when`(responseMock.setStatusCode(statusCaptor.capture())).thenReturn(responseMock)
        `when`(responseMock.end(messageCaptor.capture())).doReturn(succeededFuture())
    }
}
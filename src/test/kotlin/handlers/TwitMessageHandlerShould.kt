package handlers

import action.TwitMessage
import domain.errors.UserNotFoundException
import infrastructure.server.handlers.TwitMessageHandler
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.`when`
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

class TwitMessageHandlerShould {
    private lateinit var statusCaptor: ArgumentCaptor<Int>
    private lateinit var messageCaptor: ArgumentCaptor<String>
    private lateinit var twitMessageNicknameCaptor: ArgumentCaptor<String>
    private lateinit var twitMessageMessageCaptor: ArgumentCaptor<String>
    private lateinit var actionMock: TwitMessage
    private lateinit var routingContextMock: RoutingContext
    private lateinit var responseMock: HttpServerResponse
    private lateinit var requestBody: String
    private lateinit var twitRequest: TwitMessageHandler.TwitRequest

    @Before
    fun initialize() {
        statusCaptor = ArgumentCaptor.forClass(Int::class.java)
        messageCaptor = ArgumentCaptor.forClass(String::class.java)
        twitMessageNicknameCaptor = ArgumentCaptor.forClass(String::class.java)
        twitMessageMessageCaptor = ArgumentCaptor.forClass(String::class.java)
        actionMock = mock()
        routingContextMock = mock()
        responseMock = mock()
        twitRequest = TwitMessageHandler.TwitRequest("@jack", "this is a message")
        requestBody = ""
    }

    @Test
    fun `return status 200 after successfully twitting a message`() {
        `given a valid twit request`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.OK.code(), "The message has been posted successfully")
    }

    @Test
    fun `return status 400 when the nickname parameter is missing`() {
        `given a twit request with no nickname`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Parameter '${ TwitMessageHandler.TwitRequest::nickname.name }' cannot be empty.")
    }

    @Test
    fun `return status 400 when the message parameter is missing`() {
        `given a twit request with no message`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.BAD_REQUEST.code(), "Parameter '${ TwitMessageHandler.TwitRequest::message.name }' cannot be empty.")
    }

    @Test
    fun `return status 404 when the the user is not found`() {
        `given a twit request with a non existent user`()

        `when the handle is invoked`()

        `then it should respond with`(HttpResponseStatus.NOT_FOUND.code(), "User with nickname '${ twitRequest.nickname }' not found.")
    }

    @Test
    fun `pass the correct values to the action`() {
        `given a valid twit request`()
        `given the parameters`()

        `when the handle is invoked`()

        `then the action should have received the correct parameters`(twitRequest.nickname, twitRequest.message)
    }

    private fun `given a valid twit request`() {
        requestBody = Json.encodePrettily(twitRequest)

        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given a twit request with no nickname`() {
        requestBody = Json.encodePrettily(twitRequest.copy(nickname = ""))

        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given a twit request with no message`() {
        requestBody = Json.encodePrettily(twitRequest.copy(message = ""))

        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given a twit request with a non existent user`() {
        requestBody = Json.encodePrettily(twitRequest)

        setupTwitMessageMockWithNotFoundException()
        setupMockRoutingContext()
        setupMockResponse()
    }

    private fun `given the parameters`() {
        doNothing().`when`(actionMock).invoke(twitMessageNicknameCaptor.captureNotNull(), twitMessageMessageCaptor.captureNotNull())
    }

    private fun `when the handle is invoked`() {
        TwitMessageHandler(actionMock).handle(routingContextMock)
    }

    private fun `then it should respond with`(statusCode: Int, responseMessage: String) {
        assertThat(statusCaptor.value).isEqualTo(statusCode)
        assertThat(messageCaptor.value).isEqualTo(responseMessage)
    }

    private fun `then the action should have received the correct parameters`(value1: String, value2: String) {
        assertThat(twitMessageNicknameCaptor.value).isEqualTo(value1)
        assertThat(twitMessageMessageCaptor.value).isEqualTo(value2)
    }

    private fun setupMockRoutingContext() {
        `when`(routingContextMock.body).thenReturn(Buffer.buffer(requestBody))
        `when`(routingContextMock.response()).thenReturn(responseMock)
    }

    private fun setupMockResponse() {
        `when`(responseMock.setStatusCode(statusCaptor.capture())).thenReturn(responseMock)
        `when`(responseMock.end(messageCaptor.capture())).doReturn(Future.succeededFuture())
    }

    private fun setupTwitMessageMockWithNotFoundException() {
        `when`(actionMock.invoke(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()))
            .doAnswer { throw UserNotFoundException("User with nickname '${ twitRequest.nickname }' not found.") }
    }
}